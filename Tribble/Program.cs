﻿using System;

namespace Tribble
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(GetTribleCount(1,49));
/*
            var hours = 300;
            int iterations = hours/12;
            long tCount = 1;
            for(var i = 0; i < iterations;i++){
                tCount*=11;
                Console.WriteLine($"{i+1}\t{tCount}");
            }
//*/
        }

        public static long GetTribleCount(int startingTribles, int hours){
            return (long)Math.Pow(11,(hours/12))*startingTribles;
        }
    }
}
